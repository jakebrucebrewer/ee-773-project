import os
import pandas as pd
import urllib2

# symbols = ['MSFT', 'MU', 'INTC',
#            'XOM', 'GE', 'CVX',
#            'C', 'BAC', 'JPM',
#            'DIS']
YAHOO_URL = 'http://ichart.finance.yahoo.com/table.csv'


def merge_data(symbols, save_loc='', save_name='merged', col='Close'):
    """
    Merges the csv's of the collected data (see collect_data()) for symbols
    `symbols` into a single csv, where the column representing each symbol is
    the `Close` (closing price) of that symbol for each day (or whatever else
    may be specified by `col`).

    Parameters
    ----------
    symbols : list of str
        The ticker symbols of the stocks to merge. Must be a subset of the
        symbols passed to the most recent collect_data() call (and will likely
        be equivalent to that list).
    save_loc : str (filepath), default=''
        The location, relative to the current working directory, to both load
        the data (csv) for each individual ticker and to save the merged csv.
        Must have the trailing '/' included. Leave blank if the current working
        directory should be used.
    save_name : str, default='merged'
        The name under which the merged csv will be saved.
    col : str in {'Date', 'Open', 'High', 'Low', 'Close' (default), 'Volume'.
                  'Adj Close'}
        The column from each individual ticker's csv to use as the
        representative column for that ticker in the merged csv.
    """
    merged = {}
    for symb in symbols:
        data = pd.read_csv('%s%s.csv' % (save_loc, symb),
                           index_col=0, parse_dates=True)
        closing = data['Close']
        merged[symb] = closing
    merged = pd.DataFrame(merged)
    merged = merged.reindex(index=merged.index[::-1])
    merged = merged.reindex_axis(symbols, axis=1).dropna()
    merged.to_csv('%s%s.csv' % (save_loc, save_name))


def collect_data(symbols, save_loc=''):
    """
    Collects data from yahoo finance for each of the individual ticker symbols
    listed in `symbols`, saving the resulting csv's to `save_loc`.

    The call to Yahoo finance takes the pattern (while replacing AAPL with
    the desired ticker symbol):

        http://ichart.yahoo.com/table.csv?s=AAPL

    This downloads a csv from yahoo finance to `save_loc` where the rows are
    days (more specifically, every day for which the particular symbol has
    data listed on Yahoo Finance) and the columns are the `Date`,
    `Open`ing price, daily `High` price, daily `Low` price, `Close`ing price,
    `Volume` of trades, and `Adj Close` (adjusted closing price).

    Parameters
    ----------
    symbols : list of str
        A list of ticker symbols recognized by Yahoo Finance for which to grab
        data.
    save_loc : str, default=''
        The location, relative to the current working directory, to save the
        data (csv) for each individual ticker. Must have the trailing '/'
        included. Leave blank if the current working directory should be used.
    """
    d = os.path.dirname(save_loc)
    if not os.path.exists(d):
        os.makedirs(d)

    for sym in symbols:
        response = urllib2.urlopen('%s?s=%s' % (YAHOO_URL, sym))
        csv = response.read()

        csvstr = str(csv).strip("b'")

        lines = csvstr.split('\\n')
        with open('%s%s.csv' % (save_loc, sym), 'w') as f:
            for line in lines:
                f.write('%s\n' % line)
