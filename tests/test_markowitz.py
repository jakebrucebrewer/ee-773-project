from papertrader.markowitz_agent import MarkowitzAgent


def almost(left, right, tol=0.001):
    return abs(left - right) < tol


class TestMarkowitzAgent(object):

    def test_with_data(self):
        """
        Test cases and answers taken from a CS 412 homework problem.
        """

        agent = MarkowitzAgent()

        answers = {
            0: {'S&P 500': 1},
            0.1: {'S&P 500': 1},
            1: {
                'S&P 500': 0.537860,
                'EAFE': 0.101952,
                'Gold': 0.360189
            },
            2: {
                'T-Bills': 0.596322,
                'S&P 500': 0.050069,
                'Lehman Bros': 0.221304,
                'EAFE': 0.071737,
                'Gold': 0.060567
            },
            4: {
                'T-Bills': 0.636618,
                'S&P 500': 0.044743,
                'NASDAQ': 0.003321,
                'Lehman Bros': 0.196823,
                'EAFE': 0.059269,
                'Gold': 0.059227
            },
            8: {
                'T-Bills': 0.805118,
                'S&P 500': 0.025491,
                'NASDAQ': 0.030647,
                'Lehman Bros': 0.083281,
                'EAFE': 0.005346,
                'Gold': 0.050117
            },
            1024: {
                'T-Bills': 0.911269,
                'S&P 500': 0.010245,
                'Lehman Bros': 0.045950,
                'EAFE': 0.016692,
                'Gold': 0.015844
            }
        }

        for mu in [0, 0.1, 1, 2, 4, 8, 1024]:
            # Step 1: Collect Stock Data
            returns, means, stdevs, histories = \
                agent.get_stock_data(src='test')

            # Step 2: Formulate the MAD Linear Program
            problem, x, y = agent.get_mad_lp(returns, means, mu)
            # print problem

            # Step 3: Solve the MAD LP and collect results
            portfolio = agent.solve_problem(problem, x)

            # Step 4: Test Results
            print portfolio
            for (security, quantity) in portfolio.iteritems():
                expected = answers[mu].get(security, 0)
                print 'Trade on mu=%.1f: %s=%.3f, expected %.3f' % \
                    (mu, security, quantity, expected)
                assert almost(quantity, expected)
